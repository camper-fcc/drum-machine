# Drum Machine

## [freeCodeCamp challenge](https://learn.freecodecamp.org/front-end-libraries/front-end-libraries-projects/build-a-drum-machine)

## [Live Demo](https://camper-fcc.gitlab.io/drum-machine/)

## General Features

- Hosted on [GitLab Pages](https://about.gitlab.com/features/pages/)
- Uses [semantic HTML5](https://internetingishard.com/html-and-css/semantic-html/) - [fCC Guide on Semantic HTML](https://guide.freecodecamp.org/html/html5-semantic-elements)
- [GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html) Licensed

## React Features

- Built with Create React App
- Each component is responsible for rendering one thing
- Uses [Styled Components](https://www.styled-components.com/) for CSS
