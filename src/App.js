/*
  This file is part of @camper-fcc's Drum Machine.

  @camper-fcc's Drum Machine is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Drum Machine is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Drum Machine.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import styled, {
  ThemeProvider,
  createGlobalStyle,
  keyframes,
  css
} from "styled-components";
import Header from "./components/Header";
import Main from "./components/Main";

const theme = {
  primaryText: "rgba(144, 238, 144, 1)",
  secondaryText: "rgba(128, 128, 128, 1)"
};

const backgroundColorKeyframe = keyframes`
    0% { background-color: rgba(255, 255, 255, 0.7);  }
    10% { background-color: rgba(255, 217, 204, 1); }
    20% { background-color: rgba(255, 230, 204, 1); }
    30% { background-color: rgba(255, 242, 204, 1); }
    40% { background-color: rgba(255, 255, 204, 1); }
    50% { background-color: rgba(242, 244, 204, 1); }
    60% { background-color: rgba(230, 255, 204, 1); }
    70% { background-color: rgba(204, 255, 255, 1); }
    80% { background-color: rgba(204, 204, 255, 1); }
    90% { background-color: rgba(242, 204, 255, 1); }
    100% { background-color: rgba(255, 204, 230, 1); }
`;

const rotateBackgroundColor = props =>
  css`
    ${backgroundColorKeyframe} 30s infinite alternate;
  `;

const GlobalStyle = createGlobalStyle`
  *, *:before, *:after {
    box-sizing: border-box;
  }

  html {
    height: 100%;
  }

  body {
    margin: 0;
    height: 100%;
  }

  #root {
    height: 100%;
  }
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: auto;
  height: 100%;
  background-color: rgba(255, 255, 255, 0.7);
  animation: ${props => props.power && rotateBackgroundColor};
`;

export default class Index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      power: true
    };
  }

  handlePower(e) {
    this.setState({
      power: !this.state.power
    });
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <Container power={this.state.power}>
          <GlobalStyle />
          <Header />
          <Main power={this.state.power} onClick={e => this.handlePower(e)} />
        </Container>
      </ThemeProvider>
    );
  }
}
