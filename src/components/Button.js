/*
  This file is part of @camper-fcc's Drum Machine.

  @camper-fcc's Drum Machine is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Drum Machine is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Drum Machine.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import styled from "styled-components";

const ButtonStyle = styled.button`
  width: 80px;
  height: 80px;
  margin: 0.5rem;
  border-radius: 5px;
  font-size: 24px;
  color: ${props =>
    props.power ? props.theme.primaryText : props.theme.secondaryText};
  background-color: rgba(52, 52, 52, 1);
  border: none;
  box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.3);
`;

export default class Button extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: false
    };

    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  componentDidMount() {
    document.addEventListener("keydown", this.handleKeyPress);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeyPress);
  }

  handleClick(e) {
    const sound = document.getElementById(this.props.innerText);
    const displayText = document.getElementById("display");

    sound.currentTime = 0;
    sound.play();
    displayText.innerText = this.props.id;

    this.setState({ active: !this.state.active });
    setTimeout(() => this.setState({ active: !this.state.active }), 100);
  }

  handleKeyPress(e) {
    if (e.keyCode === this.props.innerText.charCodeAt(0) && this.props.power) {
      this.handleClick();
    }
  }

  render() {
    return (
      <ButtonStyle
        id={this.props.id}
        className={`drum-pad ${this.state.active ? "active" : ""}`}
        onClick={e => this.handleClick(e)}
        disabled={!this.props.power}
        power={this.props.power}
      >
        {this.props.innerText}
        <audio
          id={this.props.innerText}
          className="clip"
          src={this.props.soundURL}
        />
      </ButtonStyle>
    );
  }
}
