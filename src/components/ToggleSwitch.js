/*
  This file is part of @camper-fcc's Drum Machine.

  @camper-fcc's Drum Machine is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Drum Machine is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Drum Machine.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import Switch from "@material/react-switch";
import "@material/react-switch/dist/switch.css";

export default class ToggleSwitch extends Component {
  render() {
    return (
      <>
        <Switch
          nativeControlId={this.props.id}
          checked={this.props.checked}
          onClick={this.props.onClick}
          disabled={this.props.disabled}
        />
        <label htmlFor={this.props.id}>{this.props.label}</label>
      </>
    );
  }
}
