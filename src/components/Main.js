/*
  This file is part of @camper-fcc's Drum Machine.

  @camper-fcc's Drum Machine is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Drum Machine is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Drum Machine.  If not,
  see <https://www.gnu.org/licenses/>.

  Attribution
  -----------
  Drum Sounds from freeCodeCamp.

  Roaring Lion (lion.mp3):
  Mike Koenig
  http://soundbible.com/1272-Roaring-Lion.html

  TomCat (tomcat.mp3):
  Mr Smith
  http://soundbible.com/1687-TomCat.html

  Cat Scream (cat-scream.mp3)
  Ca9
  http://soundbible.com/1509-Cat-Scream.html

  Cat Meowing 2 (cat-meow-2.mp3)
  Mr Smith
  http://soundbible.com/1684-Cat-Meowing-2.html

  Cat Meow 4 (cat-meow-4.mp3)
  ZapSplat
  https://www.zapsplat.com/music/cat-kitten-meow-4/

  Angry Cat (angry-cat.mp3)
  secondbody
  http://soundbible.com/1363-Angry-Cat.html

  Cat Meowing (cat-meow.mp3)
  Mike Koenig
  http://soundbible.com/1290-Cat-Meowing.html

  Cat Meow 5 (cat-meow-3.mp3)
  ZapSplat
  https://www.zapsplat.com/music/cat-kitten-meow-5/

  Kitten Meow (kitten-meow.mp3)
  Mike Koenig
  http://soundbible.com/1286-Kitten-Meow.html
*/

import React, { Component } from "react";
import styled from "styled-components";
import PadButtons from "./PadButtons";
import ToggleSwitch from "./ToggleSwitch";
import sounds from "../data/sounds.json";

const MainContainer = styled.main`
  text-align: center;
`;

const Display = styled.div`
  width: 100%;
  font-size: 1.5rem;
  padding: 2rem;
  text-align: center;
  color: ${props =>
    props.power ? props.theme.primaryText : props.theme.secondaryText};
  background-color: rgba(0, 0, 0, 0.9);
  min-height: 6rem;
  box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.3);
`;

const DrumPad = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  margin: 1rem auto;
  max-width: 300px;

  .active {
    background-color: ${props => props.theme.secondaryText};
  }
`;

const ToggleStyle = styled.div`
  margin: 1rem 0;

  label {
    margin-left: 10px;
  }
`;

const PadTypeDisplay = styled.div`
  color: ${props =>
    props.power ? props.theme.primaryText : props.theme.secondaryText};
  width: 60%;
  margin: 0 auto;
  background-color: rgba(0, 0, 0, 0.9);
  padding: 1rem;
  box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.3);
`;

export default class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      drumPadType: "Drum Sounds",
      sounds: sounds
    };
  }

  handleToggle(e) {
    this.setState((state, props) => ({
      drumPadType:
        this.state.drumPadType === "Drum Sounds" ? "Cat Sounds" : "Drum Sounds"
    }));
  }

  render() {
    return (
      <MainContainer id="drum-machine">
        <ToggleStyle>
          <ToggleSwitch
            onClick={e => this.props.onClick(e)}
            label="Power"
            checked={this.props.power}
            id="power-switch"
          />
        </ToggleStyle>

        <Display id="display" power={this.props.power}>
          {this.props.power ? "Power On" : "Power Off"}
        </Display>

        <ToggleStyle>
          <ToggleSwitch
            onClick={e => this.handleToggle(e)}
            drumPadType={this.state.drumPadType}
            label="Choose Drum Pad"
            disabled={!this.props.power}
            id="drumPad-switch"
          />
        </ToggleStyle>

        <PadTypeDisplay power={this.props.power}>
          {this.state.drumPadType}
        </PadTypeDisplay>

        <DrumPad>
          {this.state.drumPadType === "Drum Sounds" ? (
            <PadButtons
              pad="drumSounds"
              power={this.props.power}
              sounds={this.state.sounds}
            />
          ) : (
            <PadButtons
              pad="catSounds"
              power={this.props.power}
              sounds={this.state.sounds}
            />
          )}
        </DrumPad>
      </MainContainer>
    );
  }
}
