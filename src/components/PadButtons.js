/*
  This file is part of @camper-fcc's Drum Machine.

  @camper-fcc's Drum Machine is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Drum Machine is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Drum Machine.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import Button from "./Button";

export default class PadButtons extends Component {
  renderButtons() {
    console.log(this.props.sounds[this.props.pad]);
    return this.props.sounds[this.props.pad].map(sound => (
      <Button
        key={sound.id}
        id={sound.id}
        innerText={sound.innerText}
        soundURL={sound.soundURL}
        power={this.props.power}
      />
    ));
  }

  render() {
    return <>{this.renderButtons()}</>;
  }
}
